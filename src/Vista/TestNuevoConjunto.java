/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {
    
    public static void main(String[] args) {
        
        try{
        Conjunto<Integer> c1=new Conjunto(9);
        Conjunto<Integer> c2=new Conjunto(20);
        Conjunto<Integer> c3=new Conjunto(30);
        
        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.

        c1.adicionarElemento(1);
        c1.adicionarElemento(19);
        c1.adicionarElemento(29);
        c1.adicionarElemento(5);
        c1.adicionarElemento(3);
        c1.adicionarElemento(18);
        c1.adicionarElemento(27);
        c1.adicionarElemento(8);
        c1.adicionarElemento(26);
        // error por mas elementos: c1.adicionarElemento(30);
        // error por elemento repetido: c1.adicionarElemento(1);
        
        c2.adicionarElemento(1);
        c2.adicionarElemento(2);
        c2.adicionarElemento(3);
        c2.adicionarElemento(4);
        c2.adicionarElemento(5);
        c2.adicionarElemento(6);
        c2.adicionarElemento(7);
        c2.adicionarElemento(8);
        c2.adicionarElemento(9);
        c2.adicionarElemento(10);
        c2.adicionarElemento(11);
        c2.adicionarElemento(12);
        c2.adicionarElemento(13);
        c2.adicionarElemento(14);
        c2.adicionarElemento(15);
        c2.adicionarElemento(16);
        c2.adicionarElemento(17);
        c2.adicionarElemento(18);
        c2.adicionarElemento(19);
        c2.adicionarElemento(20);
        
        c3.adicionarElemento(1);
        c3.adicionarElemento(2);
        c3.adicionarElemento(3);
        c3.adicionarElemento(4);
        c3.adicionarElemento(5);
        c3.adicionarElemento(6);
        c3.adicionarElemento(7);
        c3.adicionarElemento(8);
        c3.adicionarElemento(9);
        c3.adicionarElemento(10);
        c3.adicionarElemento(11);
        c3.adicionarElemento(12);
        c3.adicionarElemento(13);
        c3.adicionarElemento(14);
        c3.adicionarElemento(15);
        c3.adicionarElemento(16);
        c3.adicionarElemento(17);
        c3.adicionarElemento(18);
        c3.adicionarElemento(19);
        c3.adicionarElemento(20);
        c3.adicionarElemento(21);
        c3.adicionarElemento(22);
        c3.adicionarElemento(23);
        c3.adicionarElemento(24);
        c3.adicionarElemento(25);
        c3.adicionarElemento(26);
        c3.adicionarElemento(27);
        c3.adicionarElemento(28);
        c3.adicionarElemento(29);
        c3.adicionarElemento(30);
        
            //System.out.println(c1.toString());
            
            //c1.ordenar();
            //System.out.println(c1.toString());
            
            //c1.ordenarBurbuja();
            //System.out.println(c1.toString());
            
            c1.remover(19);
            System.out.println(c1.toString());
            
            //c1.concatenar(c2);
            //System.out.println(c1.toString());
        
        
        
        
        }catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    
}
